const express = require('express')
const router = express.Router()
const {  getGoals,setGoal,updateGoal,deleteGoal } = require('../controllers/goalController')

const { protect } = require('../middleware/authMiddleware')

// router.get('/',getGoals )
// router.post('/',setGoal )
// router.put('/:id', updateGoal)
// router.delete('/:id', deleteGoal)
router.get('/',protect,getGoals ).post('/',protect,setGoal )
router.put('/:id',protect, updateGoal).delete('/:id',protect, deleteGoal)

module.exports = router

