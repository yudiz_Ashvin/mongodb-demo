const mongoose = require('mongoose')


const userSchema = mongoose.Schema({
    name:{
        type: String,
        require: [true, 'Place add an name'],
        unique: true
    },
    email:{
        type: String,
        require: [true, 'Place add an email'],
        unique: true
    },
    password:{
        type: String,
        require: [true, 'Place add an password'],
        unique: true
    }
},
{
    timestamps: true
})

module.exports = mongoose.model('User', userSchema )